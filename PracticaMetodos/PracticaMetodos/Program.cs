﻿using System;

namespace PracticaMetodos
{
    class Program
    {
        static void Main(string[] args)
        {
            // Generamos numero aleatorio entre 0 y 100

            Random numero = new Random();
            int numeroAleatorio = numero.Next(0, 100);
            Boolean esEntero = false;
            int numeroIntroducido = 0;

            // Pedimos al usuario que introduzca numero entre 0 y 100

            do {
                Console.WriteLine("Introduce un numero entre 0 y 100: ");
                esEntero = Int32.TryParse(Console.ReadLine(),out numeroIntroducido);
            } while (!esEntero);

            while (numeroAleatorio != numeroIntroducido) {

                if (numeroAleatorio > numeroIntroducido){
                    Console.WriteLine("El numero introducido es menor que el numero a averiguar");
                }else{
                    Console.WriteLine("El numero introducido es mayor que el numero a averiguar");
                }

                do{
                    Console.WriteLine("Prueba de nuevo: ");
                    esEntero = Int32.TryParse(Console.ReadLine(), out numeroIntroducido);
                }while (!esEntero);
            }

            Console.WriteLine($"ENHORABUENA!! El numero aleatorio era {numeroAleatorio}");

        }
    }
}
